#gw
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.k8s.id}"

  tags {
    Name = "VPC IGW"
  }
}
#route
resource "aws_route_table" "k8s-public-rt" {
  vpc_id = "${aws_vpc.k8s.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags {
    Name = "Public Subnet RT"
  }
}
resource "aws_route_table_association" "k8s-public-rt" {
  subnet_id = "${aws_subnet.public-subnet.id}"
  route_table_id = "${aws_route_table.k8s-public-rt.id}"
}