resource "aws_security_group" "ingress-k8s" {
  vpc_id="${aws_vpc.k8s.id}"
  name = "vpc_test_k8s"
  description = "Allow incoming HTTP connections & SSH access"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks =  ["0.0.0.0/0"]
  }

  tags {
    Name = "k8s cluster SG"
  }
}

resource "aws_security_group" "ingress-efs" {
  vpc_id="${aws_vpc.k8s.id}"
  name = "ingress-efs"
  description = "Allow efs connections only to sg ingress-k8s"

  ingress {
    security_groups = ["${aws_security_group.ingress-k8s.id}"]
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
  }

  egress {
    security_groups = ["${aws_security_group.ingress-k8s.id}"]
    from_port = 0
    to_port = 0
    protocol = "-1"
  }
  tags {
    Name = "EFS SG"
  }
}