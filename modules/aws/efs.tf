resource "aws_efs_file_system" "postgresql" {
   creation_token = "postgresql"
   performance_mode = "maxIO"
   throughput_mode = "bursting"
   encrypted = "false"
 tags = {
     Name = "EFS Postgresql"
   }
 }

 resource "aws_efs_mount_target" "efs-mt" {
   file_system_id  = "${aws_efs_file_system.postgresql.id}"
   subnet_id = "${aws_subnet.efs-subnet.id}"
   security_groups = ["${aws_security_group.ingress-efs.id}"]
 }