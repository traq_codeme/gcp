resource "aws_key_pair" "traq" {
  key_name = "traq"
  public_key = "${file("${var.pub_key_path}")}"
}


resource "aws_instance" "k8s-node" {
  count = 4
  ami   = "${var.ami}"
  instance_type = "t2.medium"
  key_name = "${aws_key_pair.traq.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  private_ip = "${lookup(var.nodes_ips,count.index)}"
  vpc_security_group_ids = ["${aws_security_group.ingress-k8s.id}"]
  associate_public_ip_address = true
  source_dest_check = false

  connection {
    user = "ec2-user"
    private_key = "${file("${var.prv_key_path}")}"
  }
  provisioner "remote-exec"{
    inline = [
      "sudo hostnamectl set-hostname ${lookup(var.nodes_hostnames,count.index)}",
      "sudo echo '127.0.0.1 ${lookup(var.nodes_hostnames,count.index)}' | sudo tee -a /etc/hosts",
      "sudo yum install -y amazon-efs-utils git python-pip",
      "sudo amazon-linux-extras install -y ansible2",
      "sleep 5",
      "sudo yum install -y docker",
      "sleep 180",
      "sudo mkdir -p /db",
      "sudo mount -t efs ${aws_efs_file_system.postgresql.id}:/ /db",
      "sudo echo '10.0.1.11 node1' | sudo tee -a /etc/hosts",
      "sudo echo '10.0.1.12 node2' | sudo tee -a /etc/hosts",
      "sudo echo '10.0.1.13 node3' | sudo tee -a /etc/hosts",
      "sudo echo '10.0.1.14 node4' | sudo tee -a /etc/hosts",
      "sudo echo '10.0.1.15 node5' | sudo tee -a /etc/hosts",
      "sudo mkdir -p /root/.ssh",
      "sudo echo '${file("${var.prv_nodes_key_path}")}' | sudo tee -a /root/.ssh/id_rsa",
      "sudo echo '${file("${var.pub_nodes_key_path}")}' | sudo tee -a /root/.ssh/id_rsa.pub",
      "sudo echo '${file("${var.pub_nodes_key_path}")}' | sudo tee -a /root/.ssh/authorized_keys",
      "sudo chmod 0700 /root/.ssh",
      "sudo chmod 0600 /root/.ssh/id_rsa",
      "sudo chmod 0600 /root/.ssh/id_rsa.pub",
      "sudo perl -pi.bak -e 's/#PermitRootLogin yes/PermitRootLogin without-password/g' /etc/ssh/sshd_config",
      "sudo systemctl restart sshd",
      ]
  }
  tags {
    Name = "${lookup(var.nodes_hostnames,count.index)}"
  }
}

resource "aws_instance" "k8s-node1" {
  ami   = "${var.ami}"
  instance_type = "t2.medium"
  key_name = "${aws_key_pair.traq.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  private_ip = "${var.node1_ip}"
  vpc_security_group_ids = ["${aws_security_group.ingress-k8s.id}"]
  associate_public_ip_address = true
  source_dest_check = false

  connection {
    user = "ec2-user"
    private_key = "${file("${var.prv_key_path}")}"
  }
  provisioner "remote-exec"{
    inline = [
      "sudo hostnamectl set-hostname ${var.node1_hostname}",
      "sudo echo '127.0.0.1 ${var.node1_ip}' | sudo tee -a /etc/hosts",
      "sudo yum install -y amazon-efs-utils git python-pip",
      "sudo amazon-linux-extras install -y ansible2",
      "sleep 5",
      "sudo yum install -y docker",
      "sleep 180",
      "sudo mkdir -p /db",
      "sudo mount -t efs ${aws_efs_file_system.postgresql.id}:/ /db",
      "sudo git -c http.sslVerify=false clone https://gitlab.openmakers.pl/root/kubespray.git",
      "sudo echo '10.0.1.11 node1' | sudo tee -a /etc/hosts",
      "sudo echo '10.0.1.12 node2' | sudo tee -a /etc/hosts",
      "sudo echo '10.0.1.13 node3' | sudo tee -a /etc/hosts",
      "sudo echo '10.0.1.14 node4' | sudo tee -a /etc/hosts",
      "sudo echo '10.0.1.15 node5' | sudo tee -a /etc/hosts",
      "sudo mkdir -p /root/.ssh",
      "sudo echo '${file("${var.prv_nodes_key_path}")}' | sudo tee -a /root/.ssh/id_rsa",
      "sudo echo '${file("${var.pub_nodes_key_path}")}' | sudo tee -a /root/.ssh/id_rsa.pub",
      "sudo echo '${file("${var.pub_nodes_key_path}")}' | sudo tee -a /root/.ssh/authorized_keys",
      "sudo chmod 0700 /root/.ssh",
      "sudo chmod 0600 /root/.ssh/id_rsa",
      "sudo chmod 0600 /root/.ssh/id_rsa.pub",
      "sudo perl -pi.bak -e 's/#PermitRootLogin yes/PermitRootLogin without-password/g' /etc/ssh/sshd_config",
      "sudo systemctl restart sshd",
      "sudo pip install -r /home/ec2-user/kubespray/requirements.txt",
      #"cd /home/ec2-user/kubespray && sudo ansible-playbook -i inventory/mycluster/hosts.yml --become --become-user=root cluster.yml"
      ]
  }
  tags {
    Name = "node1"
  }
}

