# variables
variable "aws_id" {}
variable "aws_secret" {}
variable "key_name" {
  default = "traq"
}

variable "pub_key_path" {
  description = "SSH Public Key path"
  default = "~/.ssh/id_rsa.pub"
}

variable "prv_key_path" {
  description = "SSH Private Key path"
  default = "~/.ssh/id_rsa"
}

variable "pub_nodes_key_path" {
  description = "SSH Public Key path"
  default = "../ssh/id_rsa.pub"
}

variable "prv_nodes_key_path" {
  description = "SSH Private Key path"
  default = "../ssh/id_rsa"
}

variable "aws_zone" {
  description = "AWS availability zone"
  default = "us-east-2a"
}

variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for the public subnet"
  default = "10.0.1.0/24"
}
variable "node1_ip" {
    default = "10.0.1.11"
}

variable "nodes_ips" {
    default = {
        "0" = "10.0.1.12",
        "1" = "10.0.1.13",
        "2" = "10.0.1.14",
        "3" = "10.0.1.15",
    }
}

variable "node1_hostname" {
    default ="node1"
       
}

variable "nodes_hostnames" {
    default = {
        "0" = "node2"
        "1" = "node3",
        "2" = "node4"
        "3" = "node5"
    }
}
variable "efs_subnet_cidr" {
  description = "CIDR for the efs subnet"
  default = "10.0.2.0/24"
}
variable "ami" {
  description = "Amazon Linux AMI"
  default = "ami-02bcbb802e03574ba"
}

#provider
provider "aws" {
  access_key = "${var.aws_id}"
  secret_key = "${var.aws_secret}"
  region     = "us-east-2"
}
