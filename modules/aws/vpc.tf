#vpc
resource "aws_vpc" "k8s" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags {
    Name = "test-vpc-k8s"
  }
}

#subntes

resource "aws_subnet" "public-subnet" {
  vpc_id = "${aws_vpc.k8s.id}"
  cidr_block = "${var.public_subnet_cidr}"
  availability_zone = "${var.aws_zone}"

  tags {
    Name = "Public Subnet"
  }
}
resource "aws_subnet" "efs-subnet" {
  vpc_id = "${aws_vpc.k8s.id}"
  cidr_block = "${var.efs_subnet_cidr}"
  availability_zone = "${var.aws_zone}"

  tags {
    Name = "EFS Subnet"
  }
}

