resource "google_filestore_instance" "nfs" {
  name = "${var.gfs_name}"
  zone = "${format("%s","${var.region}-${var.zone}")}"
  tier = "STANDARD"
  project = "${var.project_id}"

  file_shares {
    capacity_gb = 1024
    name        = "${var.gfs_name}"
  }

  networks {
    network = "${google_compute_network.vpcone.name}"
    modes   = ["MODE_IPV4"]
    reserved_ip_range = "192.168.2.0/29"
  }
}

output "gfs_ip" {
  value = "${google_filestore_instance.nfs.networks.0.ip_addresses.0}"
}
