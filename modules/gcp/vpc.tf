resource "google_compute_network" "vpcone" {
  name      = "${var.vpc_nameone}"
  auto_create_subnetworks = false
  project = "${var.project_id}"
}

resource "google_compute_subnetwork" "subnetone" {
  name          = "${var.subnetone}"
  ip_cidr_range = "10.10.0.0/16"
  region        = "${var.region}"
  project       = "${var.project_id}"
  network       = "${google_compute_network.vpcone.name}"
  }

resource "google_compute_network" "vpctwo" {
  name     = "${var.vpc_nametwo}"
  auto_create_subnetworks = false
  project = "${var.project_id}"
}
resource "google_compute_subnetwork" "subnettwo" {
  name          = "${var.subnettwo}"
  ip_cidr_range = "192.168.1.0/24"
  region        = "${var.region}"
  project       = "${var.project_id}"
  network       = "${google_compute_network.vpctwo.name}"
}

resource "google_compute_firewall" "firewallone" {
  name    = "firewallone"
  network = "${google_compute_network.vpcone.name}"
  direction ="INGRESS"
  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22","80", "443"]
  }
  allow {
    protocol = "udp"
    ports = ["53"]
  }
  
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "firewalltwo" {
  name    = "firewalltwo"
  network = "${google_compute_network.vpctwo.name}"
  direction ="INGRESS"
  allow {
    protocol = "all"
  }
  source_ranges = ["0.0.0.0/0"]
}

# resource "google_compute_route" "nfsroute" {
#   name        = "nfsroute"
#   dest_range  = "192.168.2.0/24"
#   network     = "${google_compute_network.vpctwo.name}"
#   next_hop_ip = "192.168.1.1"
#   priority    = 100
# }

resource "google_compute_router" "router" {
  name    = "${var.router_name}"
  region  = "${var.region}"
  network = "${google_compute_network.vpcone.name}"
}

resource "google_compute_router_nat" "router_nat" {
  name       = "${var.router_nat_name}"
  router     = "${google_compute_router.router.name}"
  region     = "${var.region}"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  nat_ip_allocate_option = "AUTO_ONLY"
  min_ports_per_vm = 64
  tcp_established_idle_timeout_sec = 1200
  udp_idle_timeout_sec = 30
}