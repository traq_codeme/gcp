resource "google_compute_instance" "k8s_instance" {
  count = 5
  name         = "${lookup(var.k8s_nodes_hostnames,count.index )}"
  machine_type = "f1-micro"
  zone = "${format("%s","${var.region}-${var.zone}")}"
  project       = "${var.project_id}"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
   }
  metadata = {
      sshKeys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_jumphost_key_file)}"
    }
  network_interface {
    subnetwork      = "${google_compute_subnetwork.subnetone.name}"
    network_ip = "${lookup(var.k8s_nodes_ips,count.index )}"
  }
  network_interface {
    subnetwork      = "${google_compute_subnetwork.subnettwo.name}"
    network_ip = "${lookup(var.k8s_nodes_ips2,count.index )}"
  }

  connection {
      type        = "ssh"
      host        = "${lookup(var.k8s_nodes_ips,count.index )}"
      user        = "${var.gce_ssh_user}"
      private_key = "${file("${var.gce_ssh_prv_jumphost_key_file}")}"

      bastion_host        = "${google_compute_instance.k8s_jumphost.network_interface.0.access_config.0.nat_ip}"
      bastion_user        = "${var.gce_ssh_user}"
      bastion_private_key = "${file("~/.ssh/id_rsa")}"
  }

  provisioner "remote-exec"{
    inline = [
      "sudo echo '${file("${var.gce_ssh_pub_jumphost_key_file}")}' | sudo tee -a /root/.ssh/authorized_keys",
      "sudo apt -yqq install nfs-common",
      "sudo mkdir -p /db && sudo ls /",
      "sudo mount -t nfs ${var.gfs_ip}:/${var.gfs_name} /db && sudo df -hP",
      # "sudo apt install -y git python-pip ansible docker",
      ]
  }
}