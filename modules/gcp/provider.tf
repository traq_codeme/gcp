provider "google" {
 credentials = "${file("${var.credentials_path}")}"
 project     = "${var.project_id}"
 region      = "${var.region}"
}

# resource "google_storage_bucket" "image-store" {
#   name     = "traq-sandbox-terraform-remote-states"
#   location = "EU"
#   force_destroy = true
# }
# resource "google_storage_bucket_acl" "image-store-acl" {
#   bucket = "${google_storage_bucket.image-store.name}"
#   predefined_acl = "publicreadwrite"
# }

terraform {
  backend "gcs" {
    bucket  = "traq-sandbox-terraform-remote-states"
    prefix  = "terraform.tfstate"
  }
}
