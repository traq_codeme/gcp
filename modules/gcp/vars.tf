variable "gce_ssh_user" {
  description = "User for ssh connection.  Default: gce-user"
  default = "gc_user"
}

variable "gce_ssh_pub_key_file" {
  description = "Path to ssh rsa pub key. Default: ~/.ssh/id_rsa.pub"
  default = "~/.ssh/id_rsa.pub"
}

variable "gce_ssh_pub_jumphost_key_file" {
  description = "Path to ssh rsa pub key. Default: ssh/id_rsa.pub"
  default = "../../ssh/id_rsa.pub"
}
variable "gce_ssh_prv_jumphost_key_file" {
  description = "Path to ssh rsa pub key. Default: ssh/id_rsa"
  default = "../../ssh/id_rsa"
}

variable "region" {
  description = "GCP region. Default region: europe-west1 - Belgium"
  default = "europe-west1"
}
variable "zone" {
  description = "GCP zone. Default zone: b "
  default = "b"
}

variable "project_id" {
  description = "Project id. Default: sandbox-242510"
  default = "sandbox-242510"
}

variable "credentials_path" {
  description = "Path to json file with credentaials"
  default = "../../gcpcred.json"
}

variable "vpc_nameone" {
  description = "Name of firts vpc network: Default: devvpc"
  default = "devvpcone"
  }
variable "vpc_nametwo" {
  description = "Name of second vpc network: Default: devvpc"
  default = "devvpctwo"
  }

variable "subnetone" {
  description = "Name of vpc network. Default: devfirstnetwork"
  default = "devfirstnetwork"
}

variable "subnettwo" {
  description = "Name of vpc network. Default: devsecondnetwork"
  default = "devsecondnetwork"
}
variable "router_name" {
  description = "Name of router. Default: devrouter" 
  default =  "devrouter"
}

variable "router_nat_name" {
  description = "Name of NAT router. Default: devnatrouter" 
  default =  "devnatrouter"
}

variable "k8s_nodes_hostnames" {
  description = "Names of k8s nodes"
  type = "map"
  default = {
      "0" = "node1"
      "1" = "node2"
      "2" = "node3"
      "3" = "node4"
      "4" = "node5"
  }
}

variable "k8s_nodes_ips" {
  description = "Names of k8s prv ips"
  type = "map"
  default = {
      "0" = "10.10.0.2"
      "1" = "10.10.0.3"
      "2" = "10.10.0.4"
      "3" = "10.10.0.5"
      "4" = "10.10.0.6"
  }
}

variable "k8s_nodes_ips2" {
  description = "Names of k8s prv ips"
  type = "map"
  default = {
  "0" = "192.168.0.2"
    "1" = "192.168.0.3"
    "2" = "192.168.0.4"
    "3" = "192.168.0.5"
    "4" = "192.168.0.6"
  }
}
variable "jumphost_name" {
  description = "Name of ssh jumphost"
  default = "dev_jumphost"  
}

variable "jumphost_ip" {
  description = "Prv ip for jumphost. Default: 10.10.254.254"
  default = "10.10.254.254"
}
variable "jumphost_ip2" {
  description = "Prv ip for jumphost. Default: 192.168.1.254"
  default = "192.168.1.250"
}

variable "gfs_name" {
  description = "Google filestore (NFS share) name. Default devnfs"
  default = "devnfs"
}

variable "gfs_ip" {
  description =  "Google filestore (NFS share) ip got from gcp"
}
