resource "google_compute_instance" "k8s_jumphost" {
  name         = "${var.jumphost_name}"
  machine_type = "f1-micro"
  zone = "${format("%s","${var.region}-${var.zone}")}"
  project       = "${var.project_id}"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
   }
  metadata = {
      sshKeys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
    }
  
  network_interface {
    subnetwork      = "${google_compute_subnetwork.subnetone.name}"
    network_ip = "${var.jumphost_ip}"
    access_config {
      network_tier = "STANDARD"
    }
  }
  network_interface {
    subnetwork      = "${google_compute_subnetwork.subnettwo.name}"
    network_ip = "${var.jumphost_ip2}"
  }
  connection {
      host     = "${self.network_interface.0.access_config.0.nat_ip}"
      #port     = "22"
      type     = "ssh"
      user     = "${var.gce_ssh_user}"
      private_key = "${file("~/.ssh/id_rsa")}"
    }

provisioner "remote-exec"{
    
    inline = [
      
      #"sudo apt install -y nfs-common git python-pip ansible docker",
      # "sudo mkdir /root/.ssh",
      # "sudo echo '${file("${var.gce_ssh_prv_jumphost_key_file}")}' | sudo tee -a /root/.ssh/id_rsa",
      # "sudo echo '${file("${var.gce_ssh_pub_jumphost_key_file}")}' | sudo tee -a /root/.ssh/id_rsa.pub",
      # "sudo echo '${file("${var.gce_ssh_pub_key_file}")}' | sudo tee -a /root/.ssh/authorized_keys",
      # "sudo chmod 0600 /root/.ssh/id_rsa",
      # "sudo chmod 0600 /root/.ssh/id_rsa.pub",
      
      "sudo echo '${file("${var.gce_ssh_prv_jumphost_key_file}")}' | sudo tee -a /home/'${var.gce_ssh_user}'/.ssh/id_rsa",
      "sudo echo '${file("${var.gce_ssh_pub_jumphost_key_file}")}' | sudo tee -a /home/'${var.gce_ssh_user}'/.ssh/id_rsa.pub",
      "sudo chown '${var.gce_ssh_user}':'${var.gce_ssh_user}' /home/'${var.gce_ssh_user}'/.ssh/* -R && ls -al /home/'${var.gce_ssh_user}'/.ssh/",
      "sudo chmod 0600 /home/'${var.gce_ssh_user}'/.ssh/id_rsa",
      "sudo chmod 0600 /home/'${var.gce_ssh_user}'/.ssh/id_rsa.pub",

      # "sudo perl -pi.bak -e 's/#PermitRootLogin yes/PermitRootLogin without-password/g' /etc/ssh/sshd_config",
      # "sudo systemctl restart sshd",
      # "sudo git -c http.sslVerify=false clone https://gitlab.openmakers.pl/root/kubespray.git",
      #"sudo pip install -r /home/ec2-user/kubespray/requirements.txt",
      #"cd /home/ec2-user/kubespray && sudo ansible-playbook -i inventory/mycluster/hosts.yml --become --become-user=root cluster.yml"
      ]
  }
}


