module "dev_gcp" {
  source               = "../modules/gcp"
  gce_ssh_user         = "gc_user"
  gce_ssh_pub_key_file = "~/.ssh/id_rsa.pub"
  gce_ssh_pub_jumphost_key_file = "../../ssh/id_rsa.pub"
  gce_ssh_prv_jumphost_key_file = "../../ssh/id_rsa"
  region               = "europe-west1"
  zone                 = "b"
  project_id           = "sandbox-242510"
  credentials_path     = "../../gcpcred.json"
  vpc_nameone             = "devvpcfirts"
  vpc_nametwo             = "devvpcsecond"
  subnetone = "devfirstnetwork"
  subnettwo = "devsecondnetwork"


  k8s_nodes_hostnames = {
    "0" = "node1"
    "1" = "node2"
    "2" = "node3"
    "3" = "node4"
    "4" = "node5"
  }

  k8s_nodes_ips = {
    "0" = "10.10.0.2"
    "1" = "10.10.0.3"
    "2" = "10.10.0.4"
    "3" = "10.10.0.5"
    "4" = "10.10.0.6"
  }

  k8s_nodes_ips2 = {
    "0" = "192.168.1.2"
    "1" = "192.168.1.3"
    "2" = "192.168.1.4"
    "3" = "192.168.1.5"
    "4" = "192.168.1.6"
  }
  jumphost_name = "devjumphost"
  jumphost_ip   = "10.10.254.254"
  jumphost_ip2   = "192.168.1.250"
  gfs_ip  = "${module.dev_gcp.gfs_ip}"
  gfs_name = "devnfs"
}

output "gfs_ip" {
  value = "${module.dev_gcp.gfs_ip}"
}
