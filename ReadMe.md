# Cloud agnostinc terraform k8s installation on gcp/aws: gce/ec2

## Depencies

- account on Google Cloud Platform/Amazon Web Services
- installed localy terraform <https://learn.hashicorp.com/terraform/getting-started/install.html>

### How it works

- terraform will create infrastructure for k8s installation
- kubespray will create pre-defined k8s cluster
- terraform will exec shell script which will create k8s deployments, services, statefullstes etc...

### How to use it

- clone this repository
- generate pair ssh keys in ssh folder above this repository: `mkdir ../ssh && ssh-keygen -t rsa -f ../ssh/id_rsa -q -P ""`
- GCP: download from gcp to folder above this repository and rename json file to  `gcpcred.jcon`
- AWS: tbd
- run console command in repository: `terraform apply`

### Todo

#### GCP

- fix mount gfs with  second network - done
- create jumphost with pub ip - done
- download sh file for: mount, install kubespray
- add some examplary postgresql database k8s template
- add some examplary loadbalancer for postgresql k8s template
- create resrvations for ips

#### AWS - unfinished, dot'n use

- need to be reviewed, and refactored
